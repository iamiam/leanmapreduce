#!/usr/bin/python

import sys
import subprocess
import os
import time
import paramiko
import random
import socket
import re
import math
import shlex

port = 22
pkey_file = "/root/.ssh/id_rsa"
localip = socket.gethostbyname(socket.gethostname())

#the remote directory for the Lean MapReduce project files on the remote host
localdir = "/home/ubuntu/mapproject"
remotedir = "/home/ubuntu/mapproject/"
transfolder = "/home/ubuntu/mapproject/transfolder"
print "this is the local ip address %s" % localip

def sftp_tx():
	transport = paramiko.Transport((host, port))
        transport.connect(username=uname, pkey=pkey)
	sftp = paramiko.SFTPClient.from_transport(transport)
	if os.path.isfile(localdir):
		sftp.put(localdir, destdir)
	elif os.path.isdir(localdir):
		for item in os.listdir(localdir):
			filepath = os.path.join(localdir, item)
			remotepath = os.path.join(destdir, item)
			sftp.put(filepath,remotepath)
	return
def run(host, uname, pkey, command, other, port=None, localdir=None, remotedir=None):
	key = paramiko.RSAKey.from_private_key_file(pkey)
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	ssh.load_system_host_keys()
        ssh.connect(host, username=uname, pkey=key)
        if command == None:
		print "No command was supplied"
	else:
		try:
			stdin, stdout, stderr = ssh.exec_command(command)
			if other != None:
				time.sleep(2)
				stdin.write(other)
		
                except:
                       	print "please cross-check the command you attempted to execute", " \n <<<more info>>> \n\n", stderr
	if localdir!=None and destdir!=None:
		time.sleep(4)
		sftp_tx()
	ssh.close()
	return

	
def ping(hostname):
	command = ["ping", "-c", "1", "-w", "2",  "%s" % hostname]
	runpng = subprocess.Popen(command, stdout = subprocess.PIPE)
	pngresult = runpng.communicate()[0]
        print pngresult
	pngresult = pngresult.split("\n")
	pngtime = pngresult[1].split()
	if "time" in pngtime[-2]:
		return True
	else:
		return False
		runpng.stdout.flush()

spawnstart = None
def prepRemote(localip, k):
	os.chdir("/home/ubuntu/mln")
	copy = subprocess.call("cp /root/.ssh/id_rsa.pub /var/www/html/.", shell=True)
	try:
		subprocess.check_call("rm /root/.ssh/known_hosts", shell=True)
	except:
		print "no known_hosts file"
	subprocess.call(["cp", "leanmrb.mln", "leanmrb%d.mln" % k])
	subprocess.call(["sed", "-i", "s/ipaddress/%s/g" % localip, "leanmrb%d.mln" % k])
	subprocess.call(["sed", "-i", "s/leanmrb/leanmrb%d/g" % k, "leanmrb%d.mln" % k])
	subprocess.call(["sed", "-i", "s/downstreamb/downstreamb%d/g" % k , "leanmrb%d.mln" % k])
	fnull = open(os.devnull, 'w')
	global spawnstart
	spawnstart = time.time()
	subprocess.call(["/usr/local/bin/mln", "build","-r","-f", "leanmrb%d.mln" % k],stdout=fnull, stderr=subprocess.STDOUT, close_fds=True)
	subprocess.call(["/usr/local/bin/mln", "start", "-p", "leanmrb%d" % k], stdout=fnull, stderr=subprocess.STDOUT, close_fds=True)
	status = False
	ipaddress = None
	reg_pattern = re.compile(r"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b")
	while status==False:
        	mln_stat = subprocess.Popen(["/usr/local/bin/mln", "status", "-p", "leanmrb%d" % k, "-h", "downstreamb%d" % k], stdout=subprocess.PIPE)
        	entry = mln_stat.communicate()[0]
        	ipaddress = reg_pattern.findall(entry)
        	for i in entry.split():
                	if "downstreamb%d" % k in i and ipaddress:
                        	ipaddress = ipaddress[0]
                        	ipaddress = ipaddress.strip()
                        	print "Address found! >> ", ipaddress
                        	status = True
                	else:
				continue
	return ipaddress



def randomMAC():
        mac = [ 0xc0, 0x01, 0x0a,
                random.randint(0x00, 0x7f),
                random.randint(0x00, 0xff),
                random.randint(0x00, 0xff) ]
        return ':'.join(map(lambda x: "%02x" % x, mac))



def startSupervisors(localip, k):
	hostip = prepRemote(localip, k)
	state = False
	while state != True:
		try:
			state = ping(hostip)
			print "Trying to reach %s " % hostip
		except:
			print "the host is unreachable"
			continue
	
	print "We have a go"
	ip_list.append(hostip)

def remoteExec(hostip, l):
	#ensure all packages have been insalled. Apache2 is meant to be the last so it is safe to pin the test to it
	check = False
	while check==False:
		try:
			fnull = open(os.devnull, 'w')
			curl = subprocess.check_call(["curl", "-s", "%s" % hostip], stdout=fnull, close_fds=True)
			if curl == 0:
				check=True
			else:
				check=False
		except:
			check=False
			print "not ready ..."
	command ="mkdir mapproject"
	other = "yes\n"
	username = "ubuntu"
	key = paramiko.RSAKey.from_private_key_file(pkey_file)
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.load_system_host_keys()
        ssh.connect(hostip, username=username, pkey=key)
        stdin, stdout, stderr = ssh.exec_command(command)
	try:
        	stdin.write(other)
		time.sleep(2)
		stdin.flush()
	except:
		print "not expecting input but proceeding ..."
	time.sleep(4)
	try:
		print "transfering files to ", hostip
		scp = "scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -r /home/ubuntu/mapproject/transfolder%d/. ubuntu@%s:/home/ubuntu/mapproject/." % (l,hostip)
		scp = shlex.split(scp)
		other ="yes\n"
		transfer = subprocess.Popen(scp, stdout=subprocess.PIPE, stdin=subprocess.PIPE)
		time.sleep(2)
		transfer.stdin.write(other)
		transfer.wait()
		transfer.stdout.flush()

	except:
		print "authentication failed: Arinze, find out what the problem is"
	#command = "touch testeee"
	time.sleep(20)
	data_size = int(math.ceil(float(max_data_per_vm/(1024*1024))))
	command = "sudo screen -L -d -m python /home/ubuntu/mapproject/maprunaltBmain2.py %s %s %d" % (hostip, localip, data_size) 
	#other = None
	stdin, stdout, stderr = ssh.exec_command(command)
	time.sleep(3)
	try:
		stdin.write(other)
		stdin.flush()
	except:
		print "not expecting input but proceeding..."
	time.sleep(3)
	ssh.close()
	return

max_data_per_vm = None
if not sys.argv[1]:
        #Prompt the user to do so if they have not fed in the data split limit
        max_data_per_vm = int(raw_data("please enter the max size of data in megabytes for each VM: >>>   "))
else:
        #convert max data to bytes from megabytes
        max_data_per_vm = int(sys.argv[1]) * 1024 * 1024


print "max data size is >>> ", max_data_per_vm
starttime = time.time()
os.chdir(localdir)


availmem = 16 * 1024 * 1024 * 1024

""" 
Reserve 30 percent for otber events and caching needs. IncludeOS needs memory \
three times the size of the input data. This can be catered to globally.
"""
to_alloc = int(math.ceil((availmem - (availmem *(9.32/10.0)))))

#each vm should have memory twice the size of the size of its local data

max_vm_mem_size = max_data_per_vm * 3
filesize = os.path.getsize("/home/ubuntu/mapproject/bigfile2.txt")
numof_supervisors = None

#sentinel variable to trigger cluster growth
greater = False
#file counter of input splits to be fed to local mappers
ip_list = []

if to_alloc >= filesize:
        print "only one supervisor is needed "
        numof_supervisors = 1
	subprocess.call(["cp", "-r", "transfolder", "transfolder0"])
	subprocess.call(["cp", "bigfile2.txt", "transfolder0/bigfile.txt"])
	startSupervisors(localip,numof_supervisors)
	remoteExec(ip_list[0], 0)
	spawnend = time.time()
	print "writing spawn time"
        spawntime = spawnend - spawnstart
        entry = str(int(math.ceil(float(filesize/(1024**2))))).ljust(10) + str(numof_supervisors).rjust(5)  +  str(spawnstart).rjust(15) + str(spawnend).rjust(15) + str(spawntime).rjust(15)
	print "writing to file"
        spawnsup = open("/home/ubuntu/mapproject/spawnsup", 'a+')
        spawnsup.write(entry + "\n")
        spawnsup.close()


else:
        numof_supervisors = int(math.ceil(float(filesize)/to_alloc))
        print "the data size is greater than the avail mem size >>", to_alloc, filesize
        print "number of supervisors needed >> ", numof_supervisors
        greater = True
        subprocess.call(["split","-d", "--number=%d" % numof_supervisors, "bigfile2.txt", "bigfile0"])
        for x in range(numof_supervisors):
                subprocess.call(["cp", "-r", "transfolder", "transfolder%d" % x])
		subprocess.call(["mv", "bigfile00%d" % x, "transfolder%d/bigfile.txt" % x])
	for k in range(numof_supervisors):
		startSupervisors(localip, k)
	num = 0
	for hostip in ip_list:
		remoteExec(hostip, num)
		num += 1
	print "writing spawn time"
	spawnend = time.time()
	spawntime = spawnend - spawnstart
	entry = str(int(math.ceil(float(filesize/(1024**2))))).ljust(10) + str(numof_supervisors).rjust(5)  +  str(spawnstart).rjust(15) + str(spawnend).rjust(15) + str(spawntime).rjust(15)
	spawnsup = open("/home/ubuntu/mapproject/spawnsup", 'a+')
	spawnsup.write(entry + "\n")
	spawnsup.close()	


	
	
time.sleep(5)
			
#start server socket
os.chdir(localdir)        
subprocess.Popen(["python", "server3_main.py", "%s" % str(numof_supervisors)])


#ramsize = int(math.ceil(float(max_vm_mem_size)/(1024*1024)))
#subprocess.check_call("sed -i s/128/%d/g /root/IncludeOS_install/etc/qemu_cmd.sh" % ramsize, shell=True)
		


check = False
while check== False:
	if os.path.isfile("/home/ubuntu/mapproject/finalresult.txt"):
		check= True
		endtime = time.time()
		interval = endtime - starttime
		timeentry = str(int(math.ceil(float(filesize/(1024**2))))).ljust(10) +  str(starttime).rjust(5) + str(endtime).rjust(15) + str(interval).rjust(15)
		timer = open(os.path.join(localdir,"duration.txt"), "a+")
		timer.write(timeentry + "\n")
        	timer.close()

