#!/usr/bin/python

import sys
import subprocess
import os
import time
import paramiko
import random
import socket
import math
import re
import shlex

#subprocess.check_call("chmod -R 777 mapproject") 
localdir = "/home/ubuntu/mapproject/"
transfolder = "/home/ubuntu/mapproject/transfolder/"
remotedir = "/home/ubuntu/mapproject/"
port = 22
pkey_file = "/home/ubuntu/.ssh/id_rsa"
localip = sys.argv[1]
serverip = sys.argv[2]

def pre_spawn(count):
	ips = 44
	for n in range(count+1):
		if n < 10:
                        if n == count:
                                time.sleep(5)
                                break
                        os.chdir(localdir)
                        subprocess.call(["cp", "-r", "mapresult", "mapresult00%d" % n])
                        subprocess.call(["mv", "bigfile00%d" % n, "mapresult00%d/" % n])
                        os.chdir("%s/mapresult00%d" % (localdir,n))
                        subprocess.call(["sed", "-i", "s/10,0,0,42/10,0,0,%d/g" % ips, "service.cpp"])
                        subprocess.call(["sed", "-i", "s/10.0.0.%d/10,0,0,%d/g" % (ips,ips), "service.cpp"])
                        subprocess.call(["sed", "-i", "s/bigfile/bigfile00%d/g" % n, "service.cpp"])
                        subprocess.call(["sed", "-i", "/^$/d", "bigfile00%d" % n])
                        subprocess.call(["sed", "-i", r"s/$/\\/g", "bigfile00%d" % n])
                        subprocess.call(["sed", "-i", r"$ s/\\$//g", "bigfile00%d" % n])
                        subprocess.call("truncate --size=-1 bigfile00%d" % n, shell=True)
#                       subprocess.call(["sed", "-i", r"$s/\n$//g", "bigfile0%d" % n])
                        subprocess.call(["perl", "-i", "-pe", "s/bigfile00%d/`cat bigfile00%d`/ge" % (n,n), "service.cpp"])
                        subprocess.call("rm bigfile00%d" % n, shell=True)
                        fnull = open(os.devnull, 'w')
                        run = subprocess.Popen(["make"], stdout=fnull, close_fds=True)
                        run.wait()
			ips += 1
                else:
                        if n == count:
                                time.sleep(5)
                                break
                        os.chdir(localdir)
                        subprocess.call(["cp", "-r", "mapresult", "mapresult0%d" % n])
                        subprocess.call(["mv", "bigfile0%d" % n, "mapresult0%d/" % n])
                        os.chdir("%s/mapresult0%d" % (localdir,n))
                        subprocess.call(["sed", "-i", "s/10,0,0,42/10,0,0,%d/g" % ips, "service.cpp"])
                        subprocess.call(["sed", "-i", "s/10.0.0.%d/10,0,0,%d/g" % (ips,ips), "service.cpp"])
                        subprocess.call(["sed", "-i", "s/bigfile/bigfile0%d/g" % n, "service.cpp"])
                        subprocess.call(["sed", "-i", "/^$/d", "bigfile0%d" % n])
                        subprocess.call(["sed", "-i", r"s/$/\\/g", "bigfile0%d" % n])
                        subprocess.call(["sed", "-i", r"$ s/\\$//g", "bigfile0%d" % n])
                        subprocess.call("truncate --size=-1 bigfile0%d" % n, shell=True)
#                       subprocess.call(["sed", "-i", r"$s/\n$//g", "bigfile0%d" % n])
                        subprocess.call(["perl", "-i", "-pe", "s/bigfile0%d/`cat bigfile0%d`/ge" % (n,n), "service.cpp"])
                        subprocess.call("rm bigfile0%d" % n, shell=True)
                        fnull = open(os.devnull, 'w')
                        run = subprocess.call(["make"], stdout=fnull, close_fds=True)
                        ips += 1

def start_env(count):
        pre_spawn(count)
	cpucount = 0
        ips = 44
        for m in range(count + 1):
                if m==count:
                        time.sleep(8)
                        break
                if m < 10:
                        os.chdir("%s/mapresult00%d" % (localdir,m))
                        mac = randomMAC()
                        args = "/usr/bin/qemu-system-x86_64 --enable-kvm -drive file=mapresult.img,format=raw,if=ide -device virtio-net,netdev=net0,mac=%s -netdev tap,id=net0,script=/root/IncludeOS_install/etc/qemu-ifup -name includeOS%d -vga none -nographic -smp 1 -m 200" % (mac,m)
                        args = shlex.split(args)

                        print "starting up vm at 10.0.0.%d" % ips
                        fnull=open(os.devnull, 'w')
                        subprocess.Popen(["taskset", "-c", "%d" % cpucount, "/bin/bash", "OSsim.sh", "%s" % localip], stdout=fnull, close_fds=True)
                        print "started up vm at 10.0.0.%d" % ips
                        cpucount += 1
                        if cpucount == 8:
                                cpucount = 0
                else:
                        os.chdir("%s/mapresult0%d" % (localdir,m))
                        mac = randomMAC()
                        args = "/usr/bin/qemu-system-x86_64 --enable-kvm -drive file=mapresult.img,format=raw,if=ide -device virtio-net,netdev=net0,mac=%s -netdev tap,id=net0,script=/root/IncludeOS_install/etc/qemu-ifup -name includeOS%d -vga none -nographic -smp 1 -m 200" % (mac,m)
                        args = shlex.split(args)

                        print "starting up vm at 10.0.0.%d" % ips
                        fnull=open(os.devnull, 'w')
                        subprocess.Popen(["taskset", "-c", "%d" % cpucount, "/bin/bash", "OSsim.sh", "%s" % localip], stdout=fnull, close_fds=True)
                        print "started up vm 10.0.0.%d" % ips
                        cpucount += 1
                        if cpucount == 8:
                                cpucount = 0
                ips += 1


def randomMAC():
        mac = [ 0xc0, 0x01, 0x0a,
                random.randint(0x00, 0x7f),
                random.randint(0x00, 0xff),
                random.randint(0x00, 0xff) ]
        return ':'.join(map(lambda x: "%02x" % x, mac))


max_data_per_vm = None
if not sys.argv[3]:
        #Prompt the user to do so if they have not fed in the data split limit
        max_data_per_vm = int(raw_data("please enter the max size of data for each VM: >>>   "))
else:
        max_data_per_vm = int(sys.argv[3]) * 1024 * 1024


stre = ['cat', '/proc/meminfo']
stre1 = ['awk', '/MemFree:/ { mem = $2 } /^Cached:/ { cached = $2 } END { print (mem + cached) * 1024 }']

prelim = subprocess.Popen(stre, stdout=subprocess.PIPE)
getsize = subprocess.Popen(stre1, stdin=prelim.stdout,stdout=subprocess.PIPE)
availmem = getsize.communicate()[0]
availmem = float(availmem)

# Reserve 30 percent for other events and caching needs
to_alloc = int(math.ceil((availmem - (availmem *(9.32/10.0)))))

#each vm should have memory twice the size of the size of its local data

max_vm_mem_size = max_data_per_vm * 3
filesize = os.path.getsize("/home/ubuntu/mapproject/bigfile.txt")
filesize = int(math.ceil(float(filesize)))
numof_instances = None

#sentinel variable to trigger cluster growth
greater = False
#file counter of input splits to be fed to local mappers
filecnt = 0
#keep a global counter of excess input splits in case it is needed in future
excesscnt = 0

numof_instances = int(math.ceil(float(filesize) / max_data_per_vm))
os.chdir("/home/ubuntu/mapproject")
subprocess.check_call(["split","-d", "--number=%d" % numof_instances, "bigfile.txt", "bigfile0"])

time.sleep(5)

os.chdir("/home/ubuntu/")
fnull = open(os.devnull, 'w')
subprocess.check_call(["git", "clone", "https://github.com/hioa-cs/IncludeOS.git"], stdout=fnull, close_fds=True)
#subprocess.check_calll("chmod -R 777 IncludeOS")
os.chdir("/home/ubuntu/IncludeOS/src")
subprocess.check_call("/usr/bin/make clean", shell=True)
os.chdir("/home/ubuntu/IncludeOS/vmbuild")
subprocess.check_call("/usr/bin/make clean", shell=True)
os.chdir("/home/ubuntu/IncludeOS/etc")
subprocess.check_call("./install_from_bundle.sh", shell=True)
ramsize = int(math.ceil(float(max_vm_mem_size)/(1024*1024)))
subprocess.check_call("sed -i s/128/%d/g /home/ubuntu/IncludeOS_install/etc/qemu_cmd.sh" % ramsize, shell=True)


#start server socket
os.chdir("/home/ubuntu/mapproject/")
subprocess.Popen(["python", "server3_ds.py", "%s" % str(numof_instances)])

start_env(numof_instances)

final = False
while final == False:
	if os.path.isfile("/home/ubuntu/mapproject/finalresult.txt") == True:
		final = True
		time.sleep(5)

else:
	time.sleep(15)
	s = socket.socket()
	host = serverip
	port = 1337
	s.connect((host,port))
	data = open("/home/ubuntu/mapproject/finalresult.txt", "r")
	l =data.read(1024)
	while l:
        	print "sending ............##########################  \n\n"
        	s.send(l)
        	l = data.read(1024)
	data.close()
	s.close()



	
